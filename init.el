;; settings for emacs 25.1

;; load-path
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; (setq url-proxy-services '(("http" . "127.0.0.1:8080")))

;; package
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; language
(set-language-environment "Japanese")

(if (eq system-type 'windows-nt)
    ;; windows
    (progn (set-default 'buffer-file-coding-system 'utf-8-with-signature)
           (setq exec-path (append exec-path '("c:/Program Files/Git/usr/bin")))
           (when (locate-library "w32-ime")
             (w32-ime-initialize)
             (setq default-input-method "W32-IME")
             (setq-default w32-ime-mode-line-state-indicator "[--]")
             (setq w32-ime-mode-line-state-indicator-list '("[--]" "[あ]" "[--]"))
             ))
  (if (eq system-type 'darwin)
      ;; mac
      (progn (setq ns-command-modifier (quote meta))
             (setq ns-alternate-modifier (quote super))
             (global-set-key [ns-drag-file] 'ns-find-file)
             (setq ns-pop-up-frames nil))
    ;; unix
    (prefer-coding-system 'utf-8)))

;; initial window position
(setq initial-frame-alist
      '((top . 10) (left . 860) (width . 120) (height . 60)))

;; tab
(setq-default tab-width 4)
(setq default-tab-width 4)
(setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60
                        64 58 72 76 80 84 88 92 96 100 104 108 112 116 120))

;; basic settings
(setq inhibit-startup-message t)
(tool-bar-mode 0)
(menu-bar-mode 1)
(set-scroll-bar-mode 'right)
(setq default-directory "~/")
(setq kill-whole-line t)
(delete-selection-mode 1)
(setq line-number-mode t)
(setq column-number-mode t)
(show-paren-mode t)
(setq-default indent-tabs-mode nil) ;; no tabs
(global-auto-revert-mode 1) ;; sync file
(setq completion-ignore-case t) ;; file name completion
(set-default 'truncate-lines t)
(setq sort-fold-case t) ;; ignore case in sorting
(setq visible-bell t)
(setq ring-bell-function 'ignore)
(ido-mode t)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(require 'dired-x)
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 200)
(require 'saveplace)
(setq-default save-place t) ;; save cursol position in file
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(require 'ibuffer)
(setq ibuffer-default-string-mode 'alphabetic)
(setq ibuffer-formats
      '((mark modified read-only " " (name 30 30) "" (size 6 -1) "" (mode 16 16) "" filename)
        (mark " " (name 30 -1) " " filename)))
(setq dabbrev-case-fold-search nil) ;; ignore case while dabbrev searching
(autoload 'sgml-quote "sgml-mode" "Quote SGML text in region START ... END." t)
(setq browse-url-browser-function 'brouse-url-default-windows-browser)
;; (setq browse-url-browser-function 'browse-url-firefox)
(setq browse-url-firefox-program "C:/Program Files/Mozilla Firefox/firefox.exe")
;; org-mode
(setq org-todo-keywords
      '((sequence "TODO" "|" "DONE")))
(setq org-agenda-files '("~/Documents/memo.txt"))
(setq org-startup-indented t)
;; (setq org-indent-mode t)
;; (setq org-hide-leading-stars t)
;; (setq org-adapt-indentation nil)
;; (setq org-odd-levels-only t)
;; org-plantuml
(setq org-plantuml-jar-path "C:/bin/plantuml.jar")
(defun org-mode-init ()
  (org-babel-do-load-languages
   'org-babel-load-languages
   (add-to-list 'org-babel-load-languages '(plantuml . t))))
;; python
(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "--colors=Linux --profile=default"
 python-shell-prompt-regexp "In \\[[0-9]+\\]: "
 python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
 python-shell-completion-setup-code
 "from IPython.core.completerlib import module_completion"
 python-shell-completion-module-string-code
 "';'.join(module_completion('''%s'''))\n"
 python-shell-completion-string-code
 "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

;; addtional packages settings
;; redo+
(require 'redo+)
(setq undo-no-redo t)
(setq undo-limit 10000)
(setq undo-strong-limit 20000)
;; goto-chg
(require 'goto-chg)
;; auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)
(setq ac-use-menu-map t)
(global-auto-complete-mode t)
(setq ac-auto-start nil)
(setq ac-use-fuzzy t)
(ac-set-trigger-key "C-<tab>")
;; visual-regexp
(require 'visual-regexp)
;; markdown-mode
(autoload 'markdown-mode "markdown-mode" "" t)
(autoload 'gfm-mode "markdown-mode" "" t)
;; cmake-mode
(require 'cmake-mode)
;; plantuml-mode
;; (add-to-list 'auto-mode-alist '("\\.uml$" . plantuml-mode))
;; (setq plantuml-jar-path "C:/bin/plantuml.jar")
;; (require 'plantuml-mode)

;; custom functions and settings
(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point))))
(defun delete-line ()
  "Delete text from current position to end of line char.
delete char if current position is beginning of line.
This command does not push text to `kill-ring'."
  (interactive)
  (progn (delete-region
          (point)
          (progn (end-of-line 1) (point)))
         (if (bolp) (delete-char 1))))
(defun delete-word-or-unify-spaces (arg)
  "Delete characters forward until encountering the end of a word or unifiy multiple spaces.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (if (looking-at "  +") (delete-region (match-beggining 0) (- (match-end 0) 1))
    (delete-region
     (point)
     (progn
       (forward-word arg)
       (point)))))

;; treat underscore as word
(defun jo-int-function()
  ""
  (modify-syntax-entry ?_ "w" c-mode-syntax-table)
  (modify-syntax-entry ?_ "w" c++-mode-syntax-table))
(add-hook 'c-initialization-hook 'jo-int-function)

;; custom-c-style
(defconst custom-bsd
  `("bsd"
    (c-basic-offset . 4)
    (c-offsets-alist . ((innamespace . [0])))))

;; doxygen
(fset 'doxy_param_in "@param[in]")
(fset 'doxy_param_out "@param[out]")
(fset 'doxy_param_inout "@param[in, out]")
(fset 'doxy_brief "@brief")
(fset 'doxy_return "@return")

;; key bindings
(define-key global-map (kbd "C-h") 'delete-backward-char)
(define-key global-map (kbd "C-o") 'other-window)
(define-key global-map (kbd "C-m") 'newline-and-indent)
(define-key global-map (kbd "C-j") 'newline)
(define-key global-map (kbd "C-z") nil) ;; disable minimizing
(define-key global-map (kbd "C-x C-o") 'occur)
(define-key global-map (kbd "M-r") 'recentf-open-files) ;; recentf
(define-key global-map (kbd "C-x C-b") 'ibuffer) ;; ibuffer
;; calc
(define-key global-map (kbd "C-c c") 'calc)
;; (define-key global-map (kbd "M-#") 'calculator) ;; calculator
;; dabbrev
;; (define-key global-map (kbd "C-<tab>") 'dabbrev-expand)
(define-key minibuffer-local-map (kbd "C-<tab>") 'dabbrev-expand)
(define-key global-map (kbd "C-?") 'redo) ;; redo+
;; goto-chg
(define-key global-map (kbd "M-o") 'goto-last-change)
(define-key global-map (kbd "S-M-o") 'goto-last-change-reverse)
;; auto-complete
(define-key ac-completing-map (kbd "C-<tab>") 'ac-expand)
(define-key ac-menu-map (kbd "C-m") 'ac-stop)
;; whitespace cleanup
(define-key global-map (kbd "M-<return>") 'whitespace-cleanup)
;; for custom functions
(define-key global-map (kbd "M-d") 'delete-word-or-unify-spaces)
(define-key global-map (kbd "C-k") 'delete-line)
;; visual-regexp
(global-set-key (kbd "M-%") 'vr/query-replace)
;; org-mode
(define-key global-map (kbd "C-c a") 'org-agenda)
;; magit
(define-key global-map (kbd "C-x g") 'magit-status)


;; associate major mode with files
(setq auto-mode-alist
      (append
       '(
         ("\\.txt$" . org-mode)
         ("\\.h$" . c++-mode)
         ("\\.cpp$" . c++-mode)
         ("\\.m$" . objc-mode)
         ("\\.mm$" . objc-mode)
         ("\\.cs$" . csharp-mode)
         ("\\.md$" . gfm-mode)
         )
       auto-mode-alist))

;; hook
;; dired-mode
(add-hook 'dired-mode-hook
          '(lambda()
             (define-key dired-mode-map (kbd "C-o") 'other-window)
             ))
;; emacs-lisp-mode
(add-hook 'emacs-lisp-mode-hook
          '(lambda()
             (define-key emacs-lisp-mode-map (kbd "C-c C-c") 'comment-region)
             ))
;; lisp-interaction-mode
(add-hook 'lisp-interaction-mode-hook
          '(lambda()
             (define-key lisp-interaction-mode-map (kbd "C-c C-c") 'comment-region)
             ))
;; c++-mode
(add-hook 'c++-mode-hook
          '(lambda()
             (define-key c++-mode-map (kbd "C-c C-c") 'comment-region)
             (define-key c++-mode-map (kbd "C-<return>") 'c-indent-new-comment-line)
             (define-key c++-mode-map (kbd "C-c C-i") 'doxy_param_in)
             (define-key c++-mode-map (kbd "C-c C-o") 'doxy_param_out)
             (define-key c++-mode-map (kbd "C-c C-u") 'doxy_param_inout)
             (define-key c++-mode-map (kbd "C-c C-b") 'doxy_param_brief)
             (define-key c++-mode-map (kbd "C-c C-n") 'doxy_param_return)
             (linum-mode)
             (c-add-style "custom-bsd" custom-bsd t)
             (auto-complete-mode)
             ))
;; csharp-mode
(add-hook 'csharp-mode-hook
          '(lambda()
             (setq comment-column 40)
             (setq c-basic-offset 4)
             (c-set-offset 'substatement-open 0)
             (c-set-offset 'case-label '+)
             (c-set-offset 'arglist-intro '+)
             (c-set-offset 'arglist-close 0)
             (define-key csharp-mode-map (kbd "<C-tab>") 'dabbrev-expand)
             (linum-mode)
             (auto-complete-mode)
             ))
;; org-mode
(add-hook 'org-mode-hook
          '(lambda()
             (define-key org-mode-map (kbd "C-<tab>") 'ac-expand) ;; re-define due to overridden by org-mode
             (define-key org-mode-map (kbd "C-M-b") 'org-metaleft)
             (define-key org-mode-map (kbd "C-M-f") 'org-metaright)
             (define-key org-mode-map (kbd "C-'") nil)
             (define-key org-mode-map (kbd "C-M-'") 'org-cycle-agenda-files)
             (auto-complete-mode)
             (org-mode-init)
             ))
;; markdown-mode
(add-hook 'markdown-mode-hook
          '(lambda()
             (auto-complete-mode)
             ))
;; plantul-mode
(add-hook 'plantuml-mode-hook
          '(lambda()
             (define-key plantuml-mode-map (kbd "C-<tab>") 'dabbrev-expand)
             ))
;; cmake-mode
(add-hook 'cmake-mode-hook
          '(lambda()
             (auto-complete-mode)
             (setq cmake-tab-width 4)
             ))

;; TIPS
;;
;; font size
;; C-x C-+ 文字の拡大
;; C-x C-- 文字の縮小
;; C-x C-0 標準に戻す
;;
;; register
;; ポイント位置をレジスタrに保存する（point-to-register）
;; C-x r SPC r
;; レジスタrに保存した位置に移動する（jump-to-register）
;; C-x r j r
;; レジスタrにリージョンをコピーする（copy-to-register）
;; C-x r s r
;; レジスタrからテキストを挿入する（insert-register）
;; C-x r i r
;; 数値numberをレジスタregに保存する （number-to-register）
;; C-u number C-x r n reg
;; レジスタreg内の数値をnumberだけ増やす （increment-register）
;; C-u number C-x r + reg
;; レジスタregの数値をバッファに挿入する
;; C-x r g reg
;;
;; HTML 内の <, > ,& を quote
;; M-x sgml-quote
;;
;; naming macro
;; M-x name-last-kbd-macro
;; save macro
;; M-x insert-kbd-macro

(fset 'mymacro1 nil)
(fset 'mymacro1 nil)
(define-key global-map (kbd "C-c C-1") 'mymacro1)
(define-key global-map (kbd "C-c C-2") 'mymacro2)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (deeper-blue)))
 '(package-selected-packages
   (quote
    (speed-type magit cmake-mode redo+ markdown-mode goto-chg fuzzy csharp-mode auto-complete)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family #("ＭＳ ゴシック" 0 7 (charset cp932-2-byte)) :foundry "outline" :slant normal :weight normal :height 120 :width normal)))))
